return {
  "nvim-lualine/lualine.nvim",
  dependencies = {
    { "nvim-tree/nvim-web-devicons" },
    { "archibate/lualine-time" },
  },
  lazy = false,
  priority = 900,
  config = function()
    require("lualine").setup({
      options = {
        theme = "auto",
        section_separators = "",
        component_separators = "",
        disabled_filetypes = { "undotree", "undotreeDiff" },
      },
      sections = {
        lualine_a = {
          {
            "mode",
          },
        },
        lualine_b = { { "branch", icon = "" } },
        lualine_c = {
          { "filename", path = 1 },
          { "diagnostics", sources = { "nvim_diagnostic" } },
        },
        lualine_x = { "encoding" },
        lualine_y = { "progress" },
        lualine_z = { "location" },
      },
      extensions = {
        "fugitive",
        "fzf",
        "lazy",
        "man",
        "mundo",
        "neo-tree",
        "nvim-dap-ui",
        "overseer",
        "quickfix",
      },
    })
  end,
}
