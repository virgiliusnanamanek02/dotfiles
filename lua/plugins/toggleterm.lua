return {
  {
    "akinsho/toggleterm.nvim",
    version = "*",
    opts = {
      open_mapping = [[<c-\>]],
      direction = "horizontal", -- 'vertical' | 'horizontal' | 'tab' | 'float',
    },
    config = true,
  },
}
