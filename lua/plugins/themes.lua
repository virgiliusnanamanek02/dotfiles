return {
  { "sainnhe/sonokai", priority = 1000, lazy = false },

  {
    "LazyVim/LazyVim",
    opts = {
      colorscheme = "sonokai",
    },
  },
}
